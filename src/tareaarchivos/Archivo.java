package tareaarchivos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

public class Archivo {
    // Nombre de los archivos. 
    // archivoLectura: se utiliza para guardar los 6 numeros iniciales.
    // archivoEscritura: se utiliza para guardar los resultados de la división y orden. 
    private String archivoLectura;
    private String archivoEscritura;

    // Constructor recibe el nombre del archivo de lectura y da por defecto el nombre de "resultados" al archivo de escritura.
    // Eso puede ser cambiado a que ambos tengan nombre por defecto o que ambos se reciban como parámetros.
    public Archivo(String archivoLectura) {
        this.archivoLectura = archivoLectura;
        this.archivoEscritura = "resultados";
    }

    public String getNombre() {
        return archivoLectura;
    }

    public void setNombre(String nombre) {
        this.archivoLectura = nombre;
    }
    
    // Elimina el archivo existente de lectura inicial, para guardar los números y que esto no genere duplicación.
    public boolean limpiarNumeros(){
        try{
            // Clase File. Constructor recibe como parámetro el nombre del archivo y su ubicación.
            // Por defecto todas las ubicaciones de los archivos manejados están en la raiz del proyecto
            // Desde donde se ejecuta.
            File file = new File(this.archivoLectura+".txt");
            
            // Método delete elimina el archivo mediante la ruta especificada anteriormente y retorna
            // un boolean para entender si esta operación se concretó de manera adecuada (true) o falló (false).
            if(file.delete()){
                return true;
            }else{
                return false;
            }
    	}catch(Exception e){
            e.printStackTrace();
            return false;
    	}
    }
    
    // Mista lógica que el método anterior, pero sobre el archivo de resultados.
    public boolean limpiarResultados(){
        try{
            File file = new File(this.archivoEscritura+".txt");

            if(file.delete()){
                return true;
            }else{
                return false;
            }
    	}catch(Exception e){
            e.printStackTrace();
            return false;
    	}
    }
    
    // Método principal que es invocado desde el menu (opción 2).
    public void operar(){
        ArrayList<Double> numerosLeidos = leer();
        ArrayList<Double> numerosDivididos = dividir(numerosLeidos);
        ArrayList<Double> numerosOrdenados = ordenar(numerosDivididos);
        this.limpiarResultados();
        for(int i=0;i<numerosOrdenados.size();i++){
            this.escribir(numerosOrdenados.get(i),false);
        } 
    }
    
    public ArrayList<Double> leer(){
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        ArrayList<Double> numeros = new ArrayList<Double>();
        try {
            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            archivo = new File (this.archivoLectura+".txt");
            fr = new FileReader (archivo);
            br = new BufferedReader(fr);

            // Lectura del fichero
            String linea;
            while((linea=br.readLine())!=null){
                numeros.add(Double.parseDouble(linea));
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }finally{
            // En el finally cerramos el fichero, para asegurarnos
            // que se cierra tanto si todo va bien como si salta 
            // una excepcion.
            try{                    
                if( null != fr ){   
                    fr.close();     
                }                  
            }catch (Exception e2){ 
                e2.printStackTrace();
            }
        }
        return numeros;
    }
       
    // Método que toma la lista de números, la recorre y, por cada número, se realiza la operación
    // De dividir por 3 para que los resultados sean incorporados a una lista auxiliar para ser retornada.
    private ArrayList<Double> dividir(ArrayList<Double> numeros){
        
        ArrayList<Double> auxiliar = new ArrayList<Double>();
        for(int i=0; i<numeros.size();i++){
            auxiliar.add(numeros.get(i)/3);
        }
        return auxiliar;
    }
    
    private ArrayList<Double> ordenar(ArrayList<Double> numeros){
        Collections.sort(numeros, Collections.reverseOrder());
        return numeros;
    }
     
    public void escribir(double numero, boolean modo){
        // Modo true ocupa escribir para el archivo que se utiliza para operar.
        // Modo false ocupa escribir para el archivo de resultados.
        FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            if(modo){
                fichero = new FileWriter(this.archivoLectura+".txt", true);
            }else{
                fichero = new FileWriter(this.archivoEscritura+".txt", true);
            }
            pw = new PrintWriter(fichero);
            pw.println(""+numero);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           // Nuevamente aprovechamos el finally para 
           // asegurarnos que se cierra el fichero.
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    }
}
