package tareaarchivos;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TareaArchivos {

    public static void main(String[] args) {
        // Definición de variables
        boolean menu = true;
        int opcion;
        Scanner sc = new Scanner(System.in);
        Archivo ar = new Archivo("numeros");
        while(menu){
            menu();
            opcion = sc.nextInt();
            switch(opcion){
                case 1:
                    System.out.println("Vamos a ingresar 6 números");
                    ar.limpiarNumeros();
                    for(int i=1;i<7;i++){
                        System.out.println("N° "+i);
                        try{
                            ar.escribir(sc.nextInt(), true);
                        }catch(InputMismatchException e){
                            System.out.println("¡HEY! Solo números");
                            return;
                        }
                    }
                    System.out.println("¡Listo!");
                    break;
                case 2:
                    System.out.println("Vamos a operar el archivo");
                    ar.operar();
                    System.out.println("¡Listo!");
                    break;
                case 3:
                    System.out.println("¡Saliendo!");
                    menu = false;
                    break;
                default:
                    break;
                       
            }
        }       
        
    }
    public static void menu(){
        System.out.println("**********************");
        System.out.println("******* MENU *********");
        System.out.println("1. Crear archivo");
        System.out.println("2. Calcular");
        System.out.println("3. Salir");
    }
}
